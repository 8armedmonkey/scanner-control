package com.eightam.scannerctrl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class Main {

    private Thread timeCheckerThread;
    private Thread inputReceiverThread;

    private Main() {
        timeCheckerThread = new TimeCheckerThread(() -> {

            System.out.println("*** Timeout ***");

            if (inputReceiverThread != null) {
                System.out.println("Sending interrupt to InputReceiverThread.");
                inputReceiverThread.interrupt();
            }

        });

        inputReceiverThread = new InputReceiverThread(input -> {

            if (timeCheckerThread != null) {
                ((TimeCheckerThread) timeCheckerThread).cancel();
            }

        });
    }

    public static void main(String[] args) throws InterruptedException {
        Main main = new Main();
        main.run();
    }

    private void run() throws InterruptedException {
        timeCheckerThread.start();
        inputReceiverThread.start();

        timeCheckerThread.join();
        inputReceiverThread.join();
    }

    private static class TimeCheckerThread extends Thread {

        private ScheduledExecutorService executorService;
        private AtomicBoolean timeout;
        private Listener listener;

        private TimeCheckerThread(Listener listener) {
            super();
            this.executorService = newSingleThreadScheduledExecutor();
            this.timeout = new AtomicBoolean(false);
            this.listener = listener;
        }

        @Override
        public synchronized void start() {
            super.start();
            executorService.schedule(() -> timeout.set(true), 10000, MILLISECONDS);
        }

        @Override
        public void run() {
            super.run();

            while (!timeout.get() && !executorService.isShutdown()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (timeout.get()) {
                listener.onTimeout();
            }

            executorService.shutdownNow();

            System.out.println("TimeCheckerThread is finishing.");
        }

        private void cancel() {
            System.out.println("TimeCheckerThread is cancelled.");
            executorService.shutdownNow();
        }

        interface Listener {

            void onTimeout();

        }

    }

    private static class InputReceiverThread extends Thread {

        private BufferedReader reader;
        private boolean interrupted;
        private Listener listener;

        private InputReceiverThread(Listener listener) {
            this.reader = new BufferedReader(new InputStreamReader(System.in));
            this.listener = listener;
        }

        @Override
        public void run() {
            super.run();

            try {
                while (!reader.ready()) {
                    Thread.sleep(100);
                }

                String input = reader.readLine();
                listener.onInputRead(input);

            } catch (IOException e) {
                e.printStackTrace();

            } catch (InterruptedException e) {
                interrupted = true;

            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("Input receiver thread is " +
                    (interrupted ? "interrupted." : "not interrupted."));

            System.out.println("InputReceiverThread is finishing.");
        }

        interface Listener {

            void onInputRead(String input);

        }

    }

}
